function(e,r,t){let l;function i(e){let{children:r,title:t}=e;return l.createElement("div",{className:"ErrorAlert"},l.createElement("div",{className:"ErrorAlert__title"},t),r)}t.export({ErrorAlert:()=>i}),t.link("react",{default(e){l=e}},0),t.link("./ErrorAlert.css")}

