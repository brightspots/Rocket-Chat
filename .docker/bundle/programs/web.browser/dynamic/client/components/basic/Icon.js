function(e,c,n){let t;n.export({Icon:()=>a}),n.link("react",{default(e){t=e}},0);const a=e=>{let{icon:c,block:n="",baseUrl:a="",className:o}=e;return t.createElement("svg",{className:["rc-icon",n,n&&c&&"".concat(n,"--").concat(c),o].filter(Boolean).join(" "),"aria-hidden":"true"},t.createElement("use",{xlinkHref:"".concat(a,"#icon-").concat(c)}))}}

