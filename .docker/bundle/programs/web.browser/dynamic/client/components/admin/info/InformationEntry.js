function(e,t,n){let o;n.export({InformationEntry:()=>r}),n.link("react",{default(e){o=e}},0);const r=e=>{let{children:t,label:n}=e;return o.createElement("tr",{className:"admin-table-row"},o.createElement("th",{className:"content-background-color border-component-color"},n),o.createElement("td",{className:"border-component-color"},t))}}

