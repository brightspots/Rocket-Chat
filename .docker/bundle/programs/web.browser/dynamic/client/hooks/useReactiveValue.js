function(t,e,n){let r,c,o;n.export({useReactiveValue:()=>u}),n.link("meteor/tracker",{Tracker(t){r=t}},0),n.link("react",{useEffect(t){c=t},useState(t){o=t}},1);const u=function(t){let e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:[];const[n,u]=o(t);return c(()=>{const e=r.autorun(()=>{const e=t();u(()=>e)});return()=>{e.stop()}},e),n}}

