function(e,n,t){let i,s,r;t.export({useUserPreference:()=>u}),t.link("../../app/utils/client",{getUserPreference(e){i=e}},0),t.link("./useReactiveValue",{useReactiveValue(e){s=e}},1),t.link("./useUserId",{useUserId(e){r=e}},2);const u=function(e){let n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:void 0;const t=r();return s(()=>i(t,e,n),[t])}}

