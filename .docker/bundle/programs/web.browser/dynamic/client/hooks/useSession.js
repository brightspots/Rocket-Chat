function(e,s,i){let n,t;i.export({useSession:()=>o}),i.link("meteor/session",{Session(e){n=e}},0),i.link("./useReactiveValue",{useReactiveValue(e){t=e}},1);const o=e=>t(()=>n.get(e))}

