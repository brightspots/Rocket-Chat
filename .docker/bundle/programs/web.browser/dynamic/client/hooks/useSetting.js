function(e,t,i){let n,s;i.export({useSetting:()=>l}),i.link("../../app/settings/client",{settings(e){n=e}},0),i.link("./useReactiveValue",{useReactiveValue(e){s=e}},1);const l=e=>s(()=>n.get(e))}

