function(e,n,i){var r;i.export({Icon:function(){return a}}),i.link("react",{default:function(e){r=e}},0);var a=function(e){var n=e.icon,i=e.block,a=void 0===i?"":i,c=e.baseUrl,o=void 0===c?"":c,t=e.className;return r.createElement("svg",{className:["rc-icon",a,a&&n&&a+"--"+n,t].filter(Boolean).join(" "),"aria-hidden":"true"},r.createElement("use",{xlinkHref:o+"#icon-"+n}))}}

